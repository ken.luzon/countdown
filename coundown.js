function countdown(minutes) {
    var seconds = 60;
    var mins = minutes
    function tick() {
        var counter = document.getElementById("counter");
        var min = $('.min');
        var sec = $('.sec');
        var current_minutes = mins-1
        seconds--;
        counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
        min.text(current_minutes.toString());
        sec.text((seconds < 10 ? "0" : "") + String(seconds));
        if( seconds > 0 ) {
            setTimeout(tick, 1000);
        } else {
            setTimeout(function(){
                if(mins > 1){
                countdown(mins-1);
                }
            },1000);
        }
    }
    tick();
}
    
countdown(n);